module.exports = {
  plugins: [
    "cypress",
  ],
  env: {
    mocha: true,
    "cypress/globals": true,
  },
  rules: {
    "vuejs-accessibility/anchor-has-content": [
      "error",
      {
        components: ["Anchor"],
        accessibleChildren: ["MyAccessibleText"],
        accessibleDirectives: ["myAccessibleDirective"],
      },
    ],
  },
};
